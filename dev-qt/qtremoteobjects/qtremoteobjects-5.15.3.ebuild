# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

#KDE_ORG_COMMIT=a4f9e56975fa6ab4a1f63a9b34a4d77b1cfe4acd
#QT5_MODULE="qtbase"
inherit qt5-build

DESCRIPTION="Implementation of the remote objects for the Qt5 framework"

if [[ ${QT5_BUILD_TYPE} == release ]]; then
        KEYWORDS="amd64 arm arm64 ~hppa ppc ppc64 ~riscv ~sparc x86"
fi

IUSE="qml +ssl"

DEPEND="
	<dev-qt/qtcore-5.16
	<dev-qt/qtnetwork-5.16[ssl=]
	qml? ( <dev-qt/qtdeclarative-5.16 )

"

RDEPEND="${DEPEND}
"

src_prepare() {
	qt_use_disable_mod qml quick src/src.pro

	qt5-build_src_prepare
}

