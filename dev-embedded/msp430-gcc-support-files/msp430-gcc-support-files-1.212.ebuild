# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="MSP430 support files (headers and linker scripts)"
HOMEPAGE="http://www.ti.com/tool/msp430-gcc-opensource"
SRC_URI="https://dr-download.ti.com/software-development/ide-configuration-compiler-or-debugger/MD-LlCjWuAbzH/9.3.1.2/${P}.zip"
LICENSE="BSD"
SLOT="0"
KEYWORDS="x86 amd64"
DEPEND=""
RDEPEND=""
IUSE=""
RESTRICT="strip"
S="${WORKDIR}/msp430-gcc-support-files"

src_install() {
	dodir "/usr/msp430-elf/lib"
	insinto "/usr/msp430-elf/lib"
	doins include/*.ld

	dodir "/usr/msp430-elf/include"
	insinto "/usr/msp430-elf/include"
	doins include/*.h
}
