# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CHECKREQS_DISK_BUILD="4500M"
inherit autotools check-reqs flag-o-matic linux-info mono-env pax-utils multilib-minimal toolchain-funcs

DESCRIPTION="Mono runtime and class libraries, a C# compiler/interpreter"
HOMEPAGE="https://mono-project.com"
SRC_URI="https://github.com/mono/mono/archive/refs/tags/${P}.tar.gz
		https://github.com/Unity-Technologies/bdwgc/archive/a27eddb837d613cb4cf436405c23ce39ed16a86d.tar.gz -> bdwgc-a27eddb837d613cb4cf436405c23ce39ed16a86d.tar.gz
		https://github.com/mono/Newtonsoft.Json/archive/471c3e0803a9f40a0acc8aeceb31de6ff93a52c4.tar.gz -> Newtonsoft.Json-471c3e0803a9f40a0acc8aeceb31de6ff93a52c4.tar.gz
		https://github.com/mono/api-doc-tools/archive/5da8127af9e68c9d58a90aa9de21f57491d81261.tar.gz -> api-doc-tools-5da8127af9e68c9d58a90aa9de21f57491d81261.tar.gz
		https://github.com/mono/api-snapshot/archive/dbe583ec8e5cbc384e41f2e6fd631b01658b783d.tar.gz -> api-snapshot-dbe583ec8e5cbc384e41f2e6fd631b01658b783d.tar.gz
		https://github.com/mono/aspnetwebstack/archive/e77b12e6cc5ed260a98447f609e887337e44e299.tar.gz -> aspnetwebstack-e77b12e6cc5ed260a98447f609e887337e44e299.tar.gz
		https://github.com/mono/reference-assemblies/archive/680013abda911528f6befca67eed5669d80c85d5.tar.gz -> binary-reference-assemblies-680013abda911528f6befca67eed5669d80c85d5.tar.gz
		https://github.com/mono/bockbuild/archive/1c3bc7a1d43557e47fbc91f38da250ab7506815e.tar.gz -> bockbuild-1c3bc7a1d43557e47fbc91f38da250ab7506815e.tar.gz
		https://github.com/mono/boringssl/archive/78b1f44750fdd72a40f8249e1d0ba00e9037d5e7.tar.gz -> boringssl-78b1f44750fdd72a40f8249e1d0ba00e9037d5e7.tar.gz
		https://github.com/mono/cecil/archive/8021f3fbe75715a1762e725594d8c00cce3679d8.tar.gz -> cecil-8021f3fbe75715a1762e725594d8c00cce3679d8.tar.gz
		https://github.com/mono/cecil/archive/33d50b874fd527118bc361d83de3d494e8bb55e1.tar.gz -> cecil-legacy-33d50b874fd527118bc361d83de3d494e8bb55e1.tar.gz
		https://github.com/mono/corefx/archive/c4eeab9fc2faa0195a812e552cd73ee298d39386.tar.gz -> corefx-c4eeab9fc2faa0195a812e552cd73ee298d39386.tar.gz
		https://github.com/mono/corert/archive/11136ad55767485063226be08cfbd32ed574ca43.tar.gz -> corert-11136ad55767485063226be08cfbd32ed574ca43.tar.gz
		https://github.com/mono/helix-binaries/archive/64b3a67631ac8a08ff82d61087cfbfc664eb4af8.tar.gz -> helix-binaries-64b3a67631ac8a08ff82d61087cfbfc664eb4af8.tar.gz
		https://github.com/mono/ikdasm/archive/f0fd66ea063929ef5d51aafdb10832164835bb0f.tar.gz -> ikdasm-f0fd66ea063929ef5d51aafdb10832164835bb0f.tar.gz
		https://github.com/mono/ikvm-fork/archive/08266ac8c0b620cc929ffaeb1f23ac37629ce825.tar.gz -> ikvm-08266ac8c0b620cc929ffaeb1f23ac37629ce825.tar.gz
		https://github.com/mono/illinker-test-assets/archive/ec9eb51af2eb07dbe50a2724db826bf3bfb930a6.tar.gz -> illinker-test-assets-ec9eb51af2eb07dbe50a2724db826bf3bfb930a6.tar.gz
		https://github.com/mono/linker/archive/ed4a9413489aa29a70e41f94c3dac5621099f734.tar.gz -> linker-ed4a9413489aa29a70e41f94c3dac5621099f734.tar.gz
		https://github.com/dotnet/llvm-project/archive/7dfdea1267f0a40955e02567dcbcd1bcb987e825.tar.gz -> llvm-project-7dfdea1267f0a40955e02567dcbcd1bcb987e825.tar.gz
		https://github.com/mono/NuGet.BuildTasks/archive/99558479578b1d6af0f443bb411bc3520fcbae5c.tar.gz -> nuget-buildtasks-99558479578b1d6af0f443bb411bc3520fcbae5c.tar.gz
		https://github.com/mono/NUnitLite/archive/a977ca57572c545e108b56ef32aa3f7ff8287611.tar.gz -> nunit-lite-a977ca57572c545e108b56ef32aa3f7ff8287611.tar.gz
		https://github.com/mono/roslyn-binaries/archive/1c6482470cd219dcc7503259a20f26a1723f20ec.tar.gz -> roslyn-binaries-1c6482470cd219dcc7503259a20f26a1723f20ec.tar.gz
		https://github.com/mono/rx/archive/b29a4b0fda609e0af33ff54ed13652b6ccf0e05e.tar.gz -> rx-b29a4b0fda609e0af33ff54ed13652b6ccf0e05e.tar.gz
		https://github.com/mono/xunit-binaries/archive/8f6e62e1c016dfb15420852e220e07091923734a.tar.gz -> xunit-binaries-8f6e62e1c016dfb15420852e220e07091923734a.tar.gz"

LICENSE="MIT LGPL-2.1 GPL-2 BSD-4 NPL-1.1 Ms-PL GPL-2-with-linking-exception IDPL"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 -riscv ~x86 ~amd64-linux"
IUSE="doc minimal nls pax-kernel selinux xen"

# Note: mono works incorrect with older versions of libgdiplus
# Details on dotnet overlay issue: https://github.com/gentoo/dotnet/issues/429
DEPEND="
	app-crypt/mit-krb5[${MULTILIB_USEDEP}]
	sys-libs/zlib[${MULTILIB_USEDEP}]
	ia64? ( sys-libs/libunwind )
	!minimal? ( >=dev-dotnet/libgdiplus-6.0.2 )
	nls? ( sys-devel/gettext )
"
RDEPEND="
	${DEPEND}
	app-misc/ca-certificates
	selinux? ( sec-policy/selinux-mono )
"
# CMake is used for bundled deps
BDEPEND="
	dev-build/cmake
	sys-devel/bc
	app-alternatives/yacc
	pax-kernel? ( sys-apps/elfix )
"

#PATCHES=(
#	"${FILESDIR}"/${PN}-5.12-try-catch.patch
#	"${FILESDIR}"/${PN}-6.12.0.122-disable-automagic-ccache.patch
#)
S="${WORKDIR}/mono-${P}"

#pkg_pretend() {
##	linux-info_pkg_setup
#
#	if use kernel_linux ; then
#		if linux_config_exists ; then
#			linux_chkconfig_builtin SYSVIPC || die "SYSVIPC not enabled in the kernel"
#		else
#			# https://github.com/gentoo/gentoo/blob/f200e625bda8de696a28338318c9005b69e34710/eclass/linux-info.eclass#L686
#			ewarn "kernel config not found"
#			ewarn "If CONFIG_SYSVIPC is not set in your kernel .config, mono will hang while compiling."
#			ewarn "See https://bugs.gentoo.org/261869 for more info."
#		fi
#	fi
#
#	# bug #687892
#	check-reqs_pkg_pretend
#}

#pkg_setup() {
#	mono-env_pkg_setup
#	check-reqs_pkg_setup
#}

src_prepare() {
	# We need to sed in the paxctl-ng -mr in the runtime/mono-wrapper.in so it don't
	# get killed in the build proces when MPROTECT is enabled, bug #286280
	# RANDMMAP kills the build process too, bug #347365
	# We use paxmark.sh to get PT/XT logic, bug #532244
	if use pax-kernel ; then
		ewarn "We are disabling MPROTECT on the mono binary."

		# issue 9 : https://github.com/Heather/gentoo-dotnet/issues/9
		sed '/exec "/ i\paxmark.sh -mr "$r/@mono_runtime@"' -i "${S}"/runtime/mono-wrapper.in || die "Failed to sed mono-wrapper.in"
	fi

	rmdir external/bdwgc && mv ../bdwgc-a27eddb837d613cb4cf436405c23ce39ed16a86d external/bdwgc
	rmdir external/binary-reference-assemblies && mv ../reference-assemblies-680013abda911528f6befca67eed5669d80c85d5 external/binary-reference-assemblies
	rmdir external/api-doc-tools && mv ../api-doc-tools-5da8127af9e68c9d58a90aa9de21f57491d81261 external/api-doc-tools
	rmdir external/api-snapshot && mv ../api-snapshot-dbe583ec8e5cbc384e41f2e6fd631b01658b783d external/api-snapshot
	rmdir external/Newtonsoft.Json && mv ../Newtonsoft.Json-471c3e0803a9f40a0acc8aeceb31de6ff93a52c4 external/Newtonsoft.Json
	rmdir external/aspnetwebstack && mv ../aspnetwebstack-e77b12e6cc5ed260a98447f609e887337e44e299 external/aspnetwebstack
	rmdir external/bockbuild && mv ../bockbuild-1c3bc7a1d43557e47fbc91f38da250ab7506815e external/bockbuild
	rmdir external/boringssl && mv ../boringssl-78b1f44750fdd72a40f8249e1d0ba00e9037d5e7 external/boringssl
	rmdir external/cecil && mv ../cecil-8021f3fbe75715a1762e725594d8c00cce3679d8 external/cecil
	rmdir external/cecil-legacy && mv ../cecil-33d50b874fd527118bc361d83de3d494e8bb55e1 external/cecil-legacy
	rmdir external/corefx && mv ../corefx-c4eeab9fc2faa0195a812e552cd73ee298d39386 external/corefx
	rmdir external/corert && mv ../corert-11136ad55767485063226be08cfbd32ed574ca43 external/corert
	rmdir external/ikdasm && mv ../ikdasm-f0fd66ea063929ef5d51aafdb10832164835bb0f external/ikdasm
	rmdir external/helix-binaries && mv ../helix-binaries-64b3a67631ac8a08ff82d61087cfbfc664eb4af8 external/helix-binaries
	rmdir external/ikvm && mv ../ikvm-fork-08266ac8c0b620cc929ffaeb1f23ac37629ce825 external/ikvm
	rmdir external/illinker-test-assets && mv ../illinker-test-assets-ec9eb51af2eb07dbe50a2724db826bf3bfb930a6 external/illinker-test-assets
	rmdir external/linker && mv ../linker-ed4a9413489aa29a70e41f94c3dac5621099f734 external/linker
	rmdir external/llvm-project && mv ../llvm-project-7dfdea1267f0a40955e02567dcbcd1bcb987e825 external/llvm-project
	rmdir external/nuget-buildtasks && mv ../NuGet.BuildTasks-99558479578b1d6af0f443bb411bc3520fcbae5c external/nuget-buildtasks
	rmdir external/nunit-lite && mv ../NUnitLite-a977ca57572c545e108b56ef32aa3f7ff8287611 external/nunit-lite
	rmdir external/roslyn-binaries && mv ../roslyn-binaries-1c6482470cd219dcc7503259a20f26a1723f20ec external/roslyn-binaries
	rmdir external/rx && mv ../rx-b29a4b0fda609e0af33ff54ed13652b6ccf0e05e external/rx
	rmdir external/xunit-binaries && mv ../xunit-binaries-8f6e62e1c016dfb15420852e220e07091923734a external/xunit-binaries

	default

	# PATCHES contains configure.ac patch
	./autogen.sh --with-mcs-docs=no || die
	#eautoreconf
	multilib_copy_sources
}

multilib_src_configure() {
	tc-ld-is-lld && filter-lto

	local myeconfargs=(
		$(use_with xen xen_opt)
		--without-ikvm-native
		--disable-dtrace
		--enable-system-aot
		$(multilib_native_use_with doc mcs-docs)
		$(use_enable nls)
	)

	# Workaround(?) for bug #779025
	# May be able to do a real fix by adjusting path used?
	if multilib_is_native_abi ; then
		myeconfargs+=( --enable-system-aot )
	else
		myeconfargs+=( --disable-system-aot )
	fi

	econf "${myeconfargs[@]}"
}

multilib_src_test() {
	emake -C mcs/tests check
}

multilib_src_install() {
	default

	# Remove files not respecting LDFLAGS and that we are not supposed to provide, see Fedora
	# mono.spec and http://www.mail-archive.com/mono-devel-list@lists.ximian.com/msg24870.html
	# for reference.
	rm -f "${ED}"/usr/lib/mono/{2.0,4.5}/mscorlib.dll.so || die
	rm -f "${ED}"/usr/lib/mono/{2.0,4.5}/mcs.exe.so || die
}

pkg_postinst() {
	# bug #762265
	cert-sync "${EROOT}"/etc/ssl/certs/ca-certificates.crt
}
