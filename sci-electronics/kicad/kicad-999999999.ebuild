# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{9..11} )

WX_GTK_VER="3.0-gtk3"

inherit check-reqs cmake optfeature python-single-r1 toolchain-funcs wxwidgets xdg-utils git-r3

DESCRIPTION="Electronic Schematic and PCB design tools"
HOMEPAGE="https://www.kicad.org"
EGIT_REPO_URI="https://git.launchpad.net/kicad"

LICENSE="GPL-2+ GPL-3+ Boost-1.0"
SLOT="0"
KEYWORDS=""
IUSE="doc examples github +ngspice occ +oce openmp +python"

REQUIRED_USE="
	python? ( ${PYTHON_REQUIRED_USE} )
	?? ( occ oce )
"

COMMON_DEPEND="
	>=dev-libs/boost-1.61:=[context,nls,threads(+)]
	media-libs/freeglut
	media-libs/glew:0=
	>=media-libs/glm-0.9.9.1
	media-libs/mesa[X(+)]
	>=x11-libs/cairo-1.8.8:=
	>=x11-libs/pixman-0.30
	x11-libs/wxGTK:${WX_GTK_VER}[X,opengl]
	github? ( net-misc/curl:=[ssl] )
	ngspice? (
		sci-electronics/ngspice[shared]
	)
	occ? ( <sci-libs/opencascade-7.5.3:=[vtk(+)] )
	oce? ( sci-libs/oce )
	python? (
		$(python_gen_cond_dep '
			>=dev-libs/boost-1.61:=[context,nls,threads(+),python,${PYTHON_USEDEP}]
			dev-python/wxpython:4.0[${PYTHON_USEDEP}]
		')
		${PYTHON_DEPS}
	)
"
DEPEND="${COMMON_DEPEND}
	python? ( >=dev-lang/swig-3.0:0 )"
RDEPEND="${COMMON_DEPEND}
	sci-electronics/electronics-menu
"
BDEPEND="doc? ( app-text/doxygen )"
CHECKREQS_DISK_BUILD="800M"

#PATCHES=(
#	"${FILESDIR}/${P}-missing-header.patch"
#	)
#	"${FILESDIR}"/"${PN}-5.1.5-help.patch"
#	"${FILESDIR}"/"${PN}-5.1.5-ninja-build.patch"
#	"${FILESDIR}"/"ldflags.patch"

pkg_setup() {
	use python && python-single-r1_pkg_setup
	use openmp && tc-check-openmp
	setup-wxwidgets
	check-reqs_pkg_setup
}


#src_unpack() {
#	git-r3_fetch
#	git-r3_checkout
#
#	if use doc; then
#		EGIT_REPO_URI="https://github.com/KiCad/kicad-doc.git"
#		git-r3_fetch
#		git-r3_checkout
#
#	fi
#
#	if use i18n; then
#		EGIT_REPO_URI="https://github.com/KiCad/kicad-i18n.git"
#		git-r3_fetch
#		EGIT_CHECKOUT_DIR="${WORKDIR}/${P}-i18n"
#		git-r3_checkout
#
#	fi
#
#	if ! use minimal; then
#		EGIT_REPO_URI="https://github.com/KiCad/kicad-library.git"
#		git-r3_fetch
#		EGIT_CHECKOUT_DIR="${WORKDIR}/${P}-library"
#		git-r3_checkout
#		mkdir "${WORKDIR}/${PN}-footprints-${PV}"
#		PRETTY_REPOS=`curl -s "https://api.github.com/orgs/KiCad/repos?per_page=99&page=1" \
#		        "https://api.github.com/orgs/KiCad/repos?per_page=99&page=2" 2> /dev/null \
#				        | sed -E 's:.+ "full_name".*"KiCad/(.+\.pretty)",:\1:p;d'`
#		for FOOTPRINT in ${PRETTY_REPOS};do
#			EGIT_REPO_URI="https://github.com/KiCad/${FOOTPRINT}"
#			git-r3_fetch
#			EGIT_CHECKOUT_DIR="${WORKDIR}/${PN}-footprints-${PV}/${FOOTPRINT}"
#			git-r3_checkout
#		done
#	fi
#
#	EGIT_REPO_URI="https://github.com/twlostow/libcontext.git"
#	git-r3_fetch
#	EGIT_CHECKOUT_DIR="${WORKDIR}/${PN}-libcontext"
#	git-r3_checkout
#
#}

src_configure() {
	xdg_environment_reset

	local mycmakeargs=(
		-DKICAD_DOCS="${EPREFIX}/usr/share/doc/${P}"
		-DKICAD_HELP="${EPREFIX}/usr/share/doc/${PN}-doc-${PV}"
		-DBUILD_GITHUB_PLUGIN="$(usex github)"
		-DKICAD_SCRIPTING="$(usex python)"
		-DKICAD_SCRIPTING_MODULES="$(usex python)"
		-DKICAD_SCRIPTING_WXPYTHON="$(usex python)"
		-DKICAD_SCRIPTING_WXPYTHON_PHOENIX="$(usex python)"
		-DKICAD_SCRIPTING_PYTHON3="$(usex python)"
		-DKICAD_SCRIPTING_ACTION_MENU="$(usex python)"
		-DKICAD_SPICE="$(usex ngspice)"
		-DKICAD_USE_OCC="$(usex occ)"
		-DKICAD_USE_OCE="$(usex oce)"
		-DKICAD_INSTALL_DEMOS="$(usex examples)"
		-DCMAKE_SKIP_RPATH="ON"
	)
	use python && mycmakeargs+=(
		-DPYTHON_DEST="$(python_get_sitedir)"
		-DPYTHON_EXECUTABLE="${PYTHON}"
		-DPYTHON_INCLUDE_DIR="$(python_get_includedir)"
		-DPYTHON_LIBRARY="$(python_get_library_path)"
	)
	if use occ; then
		if has_version "~sci-libs/opencascade-7.5.2"; then
			mycmakeargs+=(
				-DOCC_INCLUDE_DIR="${CASROOT}"/include/opencascade-7.5.2
				-DOCC_LIBRARY_DIR="${CASROOT}"/$(get_libdir)/opencascade-7.5.2
			)
		elif has_version "~sci-libs/opencascade-7.5.1"; then
			mycmakeargs+=(
				-DOCC_INCLUDE_DIR="${CASROOT}"/include/opencascade-7.5.1
				-DOCC_LIBRARY_DIR="${CASROOT}"/$(get_libdir)/opencascade-7.5.1
			)
		else
			# <occ-7.5 uses different layout
			mycmakeargs+=(
				-DOCC_INCLUDE_DIR="${CASROOT}"/include/opencascade
				-DOCC_LIBRARY_DIR="${CASROOT}"/$(get_libdir)
			)
		fi
	fi

	cmake_src_configure
}

src_compile() {
	cmake_src_compile
	if use doc; then
		cmake_src_compile dev-docs doxygen-docs
	fi
}

src_install() {
	cmake_src_install
	use python && python_optimize
	if use doc ; then
		dodoc uncrustify.cfg
		cd Documentation || die
		dodoc -r *.txt kicad_doxygen_logo.png notes_about_pcbnew_new_file_format.odt doxygen/. development/doxygen/.
	fi
}

src_test() {
	# Test cannot find library in Portage's sandbox. Let's create a link so test can run.
	ln -s "${S}_build/eeschema/_eeschema.kiface" "${S}_build/qa/eeschema/_eeschema.kiface" || die

	default
}

pkg_postinst() {
	optfeature "Component symbols library" sci-electronics/kicad-symbols
	optfeature "Component footprints library" sci-electronics/kicad-footprints
	optfeature "3D models of components " sci-electronics/kicad-packages3d
	optfeature "Project templates" sci-electronics/kicad-templates
	optfeature "Different languages for GUI" sci-electronics/kicad-i18n
	optfeature "Extended documentation" app-doc/kicad-doc
	optfeature "Creating 3D models of components" media-gfx/wings

	xdg_desktop_database_update
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}

#src_prepare() {
#	xdg_src_prepare
#
#	# Add separated out libcontext files and patch source to use them
#	mkdir -p "${S}/common/system/" || die "mkdir failed"
#	mkdir -p "${S}/include/system/" || die "mkdir failed"
#	#cp "${WORKDIR}/${PN}-libcontext/libcontext.cpp" "${S}/common/system/libcontext.cpp" || die "cp failed"
#	#cp "${WORKDIR}/${PN}-libcontext/libcontext.h" "${S}/include/system/libcontext.h" || die "cp failed"
#	## Path source to use new "built in" libcontext. Also patch libcontext.cpp to have correct include file.
#	## Path must be applied after new libcontext files have been copied to the kicad source directory.
#	#epatch "${FILESDIR}/${PN}-boost-context.patch"
#	## Patch python swig import fixer build script
#	##epatch "${FILESDIR}/${PN}-swig-import-helper.patch"
##	epatch "${FILESDIR}/${P}-glm-0.9.9.3-error.patch"
#	
#	# remove all the non unix file endings
#	edos2unix $(find "${S}" -type f -name "*.desktop")
#
#	# Remove cvpcb desktop file while it does nothing
#	#rm "${WORKDIR}/${P}/resources/linux/mime/applications/cvpcb.desktop" || die "rm failed"
#
#	# Handle optional minimal install.
#	if use minimal; then
#		# remove templates as they are not needed to run binaries
#		sed -e '/add_subdirectory( template )/d' -i CMakeLists.txt || die "sed failed"
#	else
#		# create a link to the parts library in the main project folder
#		ln -s "${WORKDIR}/${P}-library" "${S}/${PN}-library" || die "ln failed"
#		# create a link to the footprints library and add cmake build rule for it
#		#mkdir "${WORKDIR}/${PN}-footprints-${PV}"
#		ln -s "${WORKDIR}/${PN}-footprints-${PV}" "${S}/${PN}-footprints" || die "ln failed"
#		cp "${FILESDIR}/${PN}-footprints-cmakelists.txt" "${WORKDIR}/${PN}-footprints-${PV}/CMakeLists.txt" || die "cp failed"
#		# add the libraries directory to cmake as a subproject to build
#		sed "/add_subdirectory( bitmaps_png )/a add_subdirectory( ${PN}-library )" -i CMakeLists.txt || die "sed failed"
#		# add the footprints directory to cmake as a subproject to build
#		sed "/add_subdirectory( ${PN}-library )/a add_subdirectory( ${PN}-footprints )" -i CMakeLists.txt || die "sed failed"
#		# remove duplicate uninstall directions for the library module
#		sed '/make uninstall/,/# /d' -i ${PN}-library/CMakeLists.txt || die "sed failed"
#	fi
#
#	# Add internationalization for the GUI
#	if use i18n; then
#		# create a link to the translations library in the main project folder
#		ln -s "${WORKDIR}/${P}-i18n" "${S}/${PN}-i18n" || die "ln failed"
#		# Remove unused languages. Project generates only languages specified in the
#		# file in LINGUAS in the subproject folder. By default all languages are added
#		# so we sed out the unused ones based on the user linguas_* settings.
#		local lang
#		for lang in ${LANGS}; do
#			if ! use linguas_${lang}; then
#				sed "/${lang}/d" -i ${PN}-i18n/LINGUAS || die "sed failed"
#			fi
#		done
#		# cmakelists does not respect our build dir variables, so make it point to the right location
#		sed "s|\${CMAKE_BINARY_DIR}|${WORKDIR}/${P}_build|g" -i ${PN}-i18n/CMakeLists.txt || die "sed failed"
#		# we also make from the master project so the source dir is understood incorretly, replace that too
#		sed "s|\${CMAKE_SOURCE_DIR}/\${LANG}|\${CMAKE_SOURCE_DIR}/${PN}-i18n/\${LANG}|g" -i ${PN}-i18n/CMakeLists.txt || die "sed failed"
#		# add the translations directory to cmake as a subproject to build
#		sed "/add_subdirectory( bitmaps_png )/a add_subdirectory( ${PN}-i18n )" -i CMakeLists.txt || die "sed failed"
#		# remove duplicate uninstall directions for the translation module
#		sed '/make uninstall/,$d' -i ${PN}-i18n/CMakeLists.txt || die "sed failed"
#	fi
#
#	# Install examples in the right place if requested
#	if use examples; then
#		# install demos into the examples folder too
#		sed -e 's:${KICAD_DATA}/demos:${KICAD_DOCS}/examples:' -i CMakeLists.txt || die "sed failed"
#	else
#		# remove additional demos/examples as its not strictly required to run the binaries
#		sed -e '/add_subdirectory( demos )/d' -i CMakeLists.txt || die "sed failed"
#	fi
#
#	# Add important missing doc files
#	sed -e 's/INSTALL.txt/AUTHORS.txt README.txt TODO.txt/' -i CMakeLists.txt || die "sed failed"
#}
#
