# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="QUCS simulator"
HOMEPAGE="https://github.com/Qucs/qucsator"
EGIT_REPO_URI="https://github.com/Qucs/qucsator.git"
EGIT_BRANCH="develop"

LICENSE="GPL-2+ GPL-3+"
SLOT="0"
KEYWORDS=""
IUSE=""

src_compile() {
	./bootstrap
	./configure --prefix="/usr" --libdir="/usr/$(get_libdir)"
	make
}


src_install() {
	default_src_install
	find "${ED}"/usr -name 'lib*.la' -delete
}
