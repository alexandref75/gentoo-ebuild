# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8,9} )

inherit cmake git-r3 xdg-utils

DESCRIPTION="QUCS_S simulator (qt5 version)"
HOMEPAGE="https://ra3xdh.github.io/"
EGIT_REPO_URI="https://github.com/ra3xdh/qucs_s"
EGIT_BRANCH="current"

LICENSE="GPL-2+ GPL-3+"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
	sci-electronics/ngspice
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtsvg:5
	dev-qt/qtscript:5
	dev-qt/qtwidgets:5
	dev-qt/qtprintsupport:5
"

