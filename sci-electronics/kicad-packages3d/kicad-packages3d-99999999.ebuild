# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit check-reqs cmake git-r3

DESCRIPTION="Electronic Schematic and PCB design tools 3D package libraries"
HOMEPAGE="https://kicad.github.io/packages3d/"
EGIT_REPO_URI="https://github.com/KiCad/kicad-packages3D.git"

LICENSE="CC-BY-SA-4.0"
SLOT="0"
KEYWORDS=""
IUSE="occ +oce"

REQUIRED_USE="|| ( occ oce )"

DEPEND=""
RDEPEND=">=sci-electronics/kicad-99999999[occ=,oce=]"

CHECKREQS_DISK_BUILD="10G"
#S="${WORKDIR}/${P/3d/3D}"

