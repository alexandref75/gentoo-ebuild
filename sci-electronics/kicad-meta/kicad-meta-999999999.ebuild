# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Electronic Schematic and PCB design tools (meta package)"
HOMEPAGE="http://www.kicad-pcb.org"
SRC_URI=""

LICENSE="metapackage"
SLOT="0"
KEYWORDS=""
IUSE="doc minimal nls"

RDEPEND="
	>=sci-electronics/kicad-999999999
	>=sci-electronics/kicad-symbols-99999999
	>=sci-electronics/kicad-footprints-99999999
	doc? (
		>=app-doc/kicad-doc-999999999
	)
	!minimal? (
		>=sci-electronics/kicad-packages3d-99999999
		>=sci-electronics/kicad-templates-99999999
	)
	nls? (
		>=sci-electronics/kicad-i18n-99999999
	)
"
