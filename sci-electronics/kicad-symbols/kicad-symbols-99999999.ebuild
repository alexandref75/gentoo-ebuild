# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3

DESCRIPTION="Electronic Schematic and PCB design tools symbol libraries"
HOMEPAGE="https://kicad.github.io/symbols/"
EGIT_REPO_URI="https://github.com/KiCad/kicad-symbols.git"

LICENSE="CC-BY-SA-4.0"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND=">=sci-electronics/kicad-99999999"
