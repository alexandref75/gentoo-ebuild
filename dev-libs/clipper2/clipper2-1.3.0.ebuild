# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit cmake

DESCRIPTION="Polygon Clipping and Offsetting"
HOMEPAGE="https://github.com/AngusJohnson/Clipper2"

SRC_URI="https://github.com/AngusJohnson/Clipper2/archive/refs/tags/Clipper2_${PV}.tar.gz"
S="${WORKDIR}/Clipper2-Clipper2_${PV}/CPP"

LICENSE="Boost-1.0"
SLOT=0

KEYWORDS="amd64"

src_configure() {
	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=ON
		-DCLIPPER2_TESTS=OFF
		-DCLIPPER2_EXAMPLES=OFF
		-DCLIPPER2_UTILS=OFF
	)
	cmake_src_configure
}



