# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..11} )
inherit estack linux-info optfeature python-any-r1 bash-completion-r1 toolchain-funcs

MY_PV="${PV/_/-}"
MY_PV="${MY_PV/-pre/-git}"

DESCRIPTION="Tool for inspection and simple manipulation of eBPF programs and maps"
HOMEPAGE="https://github.com/libbpf/bpftool.git/"

if [[ ${PV} == 9999 ]] ; then
	EGIT_REPO_URI="https://github.com/libbpf/bpftool" 
	EGIT_BRANCH=main
	inherit git-r3
else
	LINUX_V="${PV:0:1}.x"
	LINUX_VER=$(ver_cut 1-2)
	LINUX_SOURCES="linux-${LINUX_VER}.tar.xz"
	SRC_URI=" https://www.kernel.org/pub/linux/kernel/v${LINUX_V}/${LINUX_SOURCES}"

	S_K="${WORKDIR}/linux-${LINUX_VER}"
	S="${S_K}/tools/bpf/bpftool"
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~loong ~ppc ~ppc64 ~riscv ~x86"
IUSE="caps"

RDEPEND="
	sys-libs/binutils-libs:=
	sys-libs/zlib:=
	virtual/libelf:=
	caps? ( sys-libs/libcap:= )
"
DEPEND="
	${RDEPEND}
	>=sys-kernel/linux-headers-5.8
"
BDEPEND="
	${LINUX_PATCH+dev-util/patchutils}
	${PYTHON_DEPS}
	app-arch/tar
	dev-python/docutils
"

CONFIG_CHECK="~DEBUG_INFO_BTF"

#DOCS=( README.md )

#PATCHES=(
#	"${FILESDIR}"/${PN}-1.10-python-import.patch
#)

# src_prepare() {
# 	[[ -d "${WORKDIR}"/${P}-patches ]] && PATCHES+=( "${WORKDIR}"/${P}-patches )
# 
# 	make_src_prepare
# 	python_fix_shebang ostra/ostra-cg ostra/python/ostra.py
# }
# 
# src_configure() {
# 	local mycmakeargs=( "-D__LIB=$(get_libdir)" )
# 	cmake_src_configure
# }
# # src_unpack and src_prepare are copied from dev-util/perf since
# # it's building from the same tarball, please keep it in sync with perf
# src_unpack() {
# 	local paths=(
# 		tools/bpf kernel/bpf
# 		tools/{arch,build,include,lib,perf,scripts} {scripts,include,lib} "arch/*/lib"
# 	)
# 
# 	# We expect the tar implementation to support the -j and --wildcards option
# 	echo ">>> Unpacking ${LINUX_SOURCES} (${paths[*]}) to ${PWD}"
# 	gtar --wildcards -xpf "${DISTDIR}"/${LINUX_SOURCES} \
# 		"${paths[@]/#/linux-${LINUX_VER}/}" || die
# 
# 	if [[ -n ${LINUX_PATCH} ]] ; then
# 		eshopts_push -o noglob
# 		ebegin "Filtering partial source patch"
# 		filterdiff -p1 ${paths[@]/#/-i } -z "${DISTDIR}"/${LINUX_PATCH} \
# 			> ${P}.patch
# 		eend $? || die "filterdiff failed"
# 		eshopts_pop
# 	fi
# 
# 	local a
# 	for a in ${A}; do
# 		[[ ${a} == ${LINUX_SOURCES} ]] && continue
# 		[[ ${a} == ${LINUX_PATCH} ]] && continue
# 		unpack ${a}
# 	done
# }
# 
# src_prepare() {
# 	default
# 
# 	if [[ -n ${LINUX_PATCH} ]] ; then
# 		pushd "${S_K}" >/dev/null || die
# 		eapply "${WORKDIR}"/${P}.patch
# 		popd || die
# 	fi
# 
# 	# dev-python/docutils installs rst2man.py, not rst2man
# 	sed -i -e 's/rst2man/rst2man.py/g' Documentation/Makefile || die
# }
# 
bpftool_make() {
	local arch=$(tc-arch-kernel)
	tc-export AR CC LD

	emake V=1 VF=1 \
		HOSTCC="$(tc-getBUILD_CC)" HOSTLD="$(tc-getBUILD_LD)" \
		EXTRA_CFLAGS="${CFLAGS}" ARCH="${arch}" BPFTOOL_VERSION="${MY_PV}" \
		prefix="${EPREFIX}"/usr \
		bash_compdir="$(get_bashcompdir)" \
		feature-libcap="$(usex caps 1 0)" \
		"$@"
}

src_compile() {
	cd src;emake V=1 VF=1 \
		prefix="${EPREFIX}"/usr \
		EXTRA_CFLAGS="${CFLAGS}" \
		feature-libcap="$(usex caps 1 0)" \
		bash_compdir="$(get_bashcompdir)"
#	bpftool_make
#	bpftool_make -C Documentation
}

src_install() {
	cd src;emake V=1 VF=1 \
		DESTDIR="${D}" \
		EXTRA_CFLAGS="${CFLAGS}" \
		feature-libcap="$(usex caps 1 0)" \
		prefix="${EPREFIX}"/usr \
		bash_compdir="$(get_bashcompdir)" \
		install
}
