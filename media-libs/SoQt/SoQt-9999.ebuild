# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake flag-o-matic git-r3

MY_P=${P/soqt/SoQt}

HOMEPAGE="https://github.com/coin3d/coin/wiki"
DESCRIPTION="GUI binding for using Coin/Open Inventor with Qt"
#SRC_URI="https://github.com/coin3d/soqt/releases/download/${MY_P}/${P}-src.tar.gz"
EGIT_REPO_URI="https://github.com/coin3d/soqt.git"

EHG_PROJECT="Coin3D"

LICENSE="GPL-2"
KEYWORDS="amd64 x86"
SLOT="0"
IUSE="debug doc +coin-iv-extensions spacenav qt5 qt6"

RDEPEND="
	>=media-libs/coin-4.0.0a_pre20180416
	qt5? (
		dev-qt/qtcore:5
		dev-qt/qtgui:5
		dev-qt/qtwidgets:5
		dev-qt/qtopengl:5
	)
	qt6? (
		dev-qt/qtbase:6[gui,opengl,widgets]
	)
	virtual/opengl
	x11-libs/libX11
	x11-libs/libXi
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	x11-base/xorg-proto
	doc? ( app-text/doxygen )
	spacenav? ( >=dev-libs/libspnav-0.2.2 )
"

#S="${WORKDIR}/soqt"

#PATCHES=(
#	"${FILESDIR}/${PN}-1.5.0-pkgconfig-partial.patch"
#	"${FILESDIR}/${P}-CMakeLists-EXPORT.patch"
#)
	#"${FILESDIR}/${PN}-omit-examples.patch"

DOCS=(AUTHORS ChangeLog FAQ HACKING NEWS README)

src_configure() {
	use debug && append-cppflags -DSOQT_DEBUG=1
	local mycmakeargs=(
		-DCMAKE_INSTALL_DOCDIR="${EPREFIX}/usr/share/doc/${PF}"
		-DSOQT_BUILD_DOCUMENTATION=$(usex doc)
		-DSOQT_USE_QT6=$(usex qt6 ON OFF)
		-DSOQT_USE_QT5=$(usex qt5 ON OFF)
		-DSOQT_BUILD_INTERNAL_DOCUMENTATION=OFF
		-DSOQT_VERBOSE=$(usex debug)
		-DCMAKE_INSTALL_MANDIR="${EPREFIX%/}/usr/share"
		-DCMAKE_SKIP_INSTALL_RPATH:BOOL=YES
		-DCMAKE_SKIP_RPATH:BOOL=YES
		-DCOIN_IV_EXTENSIONS=$(usex coin-iv-extensions ON OFF)
		-DHAVE_SPACENAV_SUPPORT=$(usex spacenav ON OFF)
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	sed -i -e "s/#include <cmath>/\/\/#include <cmath>/" ${D}/usr/include/Inventor/Qt/viewers/SoQtViewer.h
}
