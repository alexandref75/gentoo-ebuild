# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3

DESCRIPTION="The glue between Coin3D and Qt"
HOMEPAGE="https://bitbucket.org/Coin3D/soqt"

EGIT_REPO_URI="https://github.com/memononen/nanosvg.git"
#EGIT_BRANCH="nanosvg-fltk"

LICENSE="|| ( GPL-2 PEL )"
SLOT="0"
IUSE="+coin-iv-extensions spacenav"

#RDEPEND="
#	>=media-libs/coin-4.0.0a_pre20180416
#	virtual/opengl
#	dev-qt/qtcore:5
#	dev-qt/qtgui:5
#	dev-qt/qtopengl:5
#	dev-qt/qtwidgets:5
#	spacenav? ( >=dev-libs/libspnav-0.2.2 )
#"
#DEPEND="${RDEPEND}
#	virtual/pkgconfig
#"

#PATCHES=(
#	"${FILESDIR}/${PN}-1.5.0-pkgconfig-partial.patch"
#	"${FILESDIR}/${P}-CMakeLists-EXPORT.patch"
#)
	#"${FILESDIR}/${PN}-omit-examples.patch"

#DOCS=(AUTHORS ChangeLog FAQ HACKING NEWS README)

#src_configure() {
#	local mycmakeargs=(
#		-DCMAKE_INSTALL_DOCDIR="${EPREFIX%/}/usr/share/doc/${PF}"
#		-DCMAKE_INSTALL_MANDIR="${EPREFIX%/}/usr/share"
#		-DCMAKE_SKIP_INSTALL_RPATH:BOOL=YES
#		-DCMAKE_SKIP_RPATH:BOOL=YES
#		-DCOIN_IV_EXTENSIONS=$(usex coin-iv-extensions ON OFF)
#		-DHAVE_SPACENAV_SUPPORT=$(usex spacenav ON OFF)
#		-DUSE_QT5=ON
#	)
#
#	cmake_src_configure
#}
