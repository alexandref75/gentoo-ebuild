# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="a cross-platform SDR software with the aim of being bloat free and simple to use"
HOMEPAGE="https://github.com/AlexandreRouma/SDRPlusPlus"
CMAKE_MAKEFILE_GENERATOR=emake

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="https://github.com/AlexandreRouma/SDRPlusPlus.git"
	inherit git-r3
	KEYWORDS=""
else
	SRC_URI="https://github.com/AlexandreRouma/SDRPlusPlus/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="+plutosdr +airspy +airspyhf +portaudio +soapysdr +rtlsdr bladerf hackrf limesdr audiosink kg-sstv m17 meteor weather"
REQUIRED_USE=""

DEPEND="sci-libs/fftw
		media-libs/glfw
		media-libs/glew
		sci-libs/volk
		airspy? ( net-wireless/airspy )
		airspyhf? ( net-wireless/airspyhf )
		bladerf? ( net-wireless/bladerf )
		hackrf? ( net-wireless/libhackrf )
		limesdr? ( net-wireless/limesuite )
		rtlsdr? ( net-wireless/rtl-sdr )
		soapysdr? ( net-wireless/soapysdr )
		audiosink? ( media-libs/rtaudio )
		portaudio? ( media-libs/portaudio )
		plutosdr? ( net-libs/libiio
					net-libs/libad9361-iio )
		media-libs/rtaudio
		app-arch/zstd
		app-crypt/libmd
		dev-libs/libbsd
		media-libs/libglvnd
		sci-libs/fftw
		sci-libs/volk
		x11-libs/libXau
		x11-libs/libXdmcp
		x11-libs/libxcb"

RDEPEND="${DEPEND}"

PATCHES=( "${FILESDIR}/${PN}-lib.patch"
		)

src_prepare() {
	cmake_src_prepare
	eapply_user
}

src_configure() {
	local mycmakeargs=(
		"-DOPT_BUILD_AIRSPY_SOURCE=$(usex airspy ON OFF)"
		"-DOPT_BUILD_AIRSPYHF_SOURCE=$(usex airspyhf ON OFF)"
		"-DOPT_BUILD_BLADERF_SOURCE=OFF"
		"-DOPT_BUILD_FILE_SOURCE=ON"
		"-DOPT_BUILD_HACKRF_SOURCE=OFF"
		"-DOPT_BUILD_LIMESDR_SOURCE=OFF"
		"-DOPT_BUILD_SDDC_SOURCE=OFF"
		"-DOPT_BUILD_SDRPP_SERVER_SOURCE=ON"
		"-DOPT_BUILD_RFSPACE_SOURCE=OFF"
		"-DOPT_BUILD_RTL_SDR_SOURCE=$(usex rtlsdr ON OFF)"
		"-DOPT_BUILD_RTL_TCP_SOURCE=$(usex rtlsdr ON OFF)"
		"-DOPT_BUILD_SDRPLAY_SOURCE=OFF"
		"-DOPT_BUILD_SOAPY_SOURCE=$(usex soapysdr ON OFF)"
		"-DOPT_BUILD_SPYSERVER_SOURCE=OFF"
		"-DOPT_BUILD_PLUTOSDR_SOURCE=$(usex plutosdr ON OFF)"
		"-DOPT_BUILD_ANDROID_AUDIO_SINK=OFF"
		"-DOPT_BUILD_PORTAUDIO_SINK=OFF"
		"-DOPT_BUILD_NETWORK_SINK=ON"
		"-DOPT_BUILD_NEW_PORTAUDIO_SINK=$(usex portaudio ON OFF)"
		"-DOPT_BUILD_FALCON9_DECODER=OFF"
		"-DOPT_BUILD_KG_SSTV_DECODER=$(usex kg-sstv ON OFF)"
		"-DOPT_BUILD_M17_DECODER=$(usex m17 ON OFF)"
		"-DOPT_BUILD_METEOR_DEMODULATOR=$(usex meteor ON OFF)"
		"-DOPT_BUILD_RADIO=ON"
		"-DOPT_BUILD_WEATHER_SAT_DECODER=$(usex weather ON OFF)"
		"-DOPT_BUILD_DISCORD_PRESENCE=OFF"
		"-DOPT_BUILD_FREQUENCY_MANAGER=ON"
		"-DOPT_BUILD_RECORDER=ON"
		"-DOPT_BUILD_RIGCTL_SERVER=OFF"
		"-DOPT_BUILD_SCANNER=ON"
		"-DOPT_BUILD_SCHEDULER=OFF"
	)
	cmake_src_configure
}

#brew install \
#  airspy \
#  airspyhf \
#  cmake \
#  codec2 \
#  fftw \
#  glew \
#  glfw \
#  hackrf \
#  libbladerf \
#  librtlsdr \
#  portaudio \
#  rtl-sdr \
#  soapyrtlsdr \
#  volk

