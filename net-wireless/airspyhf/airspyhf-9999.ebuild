# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake udev

DESCRIPTION="User mode driver for Airspy HF+"
HOMEPAGE="http://www.airspy.com"

if [[ ${PV} == *9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/airspy/airspyhf.git"
else
	SRC_URI="https://github.com/airspy/airspyhf/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	S="${WORKDIR}/airspyone_host-${PV}"

	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="GPL-2+"
SLOT="0"
IUSE="udev"

RDEPEND="
	virtual/udev
	virtual/libusb:1"
DEPEND="${RDEPEND}"

#PATCHES=( "${FILESDIR}"/${PN}-1.0.10-remove-static-libs.patch )

src_configure() {
	local mycmakeargs=(
		-DINSTALL_UDEV_RULES=$(usex udev)
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	if use udev; then
		udev_newrules "${ED}"/etc/udev/rules.d/52-airspyhf.rules 52-airspyhf.rules
		rm -r "${ED}"/etc || die
	fi
}

pkg_postinst() {
	use udev && udev_reload
}
