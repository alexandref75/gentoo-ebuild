# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8..11} )

inherit distutils-r1 git-r3

DESCRIPTION="Python library to work with countries and languages"
HOMEPAGE="
	https://github.com/osandov/drgn.git
"
EGIT_REPO_URI="https://github.com/osandov/drgn.git"

LICENSE="BSD"
SLOT="0"

distutils_enable_tests unittest
