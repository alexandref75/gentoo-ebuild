# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

WX_GTK_VER=3.2-gtk3
inherit git-r3 flag-o-matic wxwidgets

DESCRIPTION="A mesh slicer to generate G-code for fused-filament-fabrication (3D printers)"
HOMEPAGE="http://slic3r.org"
SRC_URI=""
EGIT_REPO_URI="https://github.com/prusa3d/Slic3r.git"

LICENSE="AGPL-3 CC-BY-3.0"
SLOT="0"
#KEYWORDS="~amd64 ~x86"
KEYWORDS=""
IUSE="+gui test miniz qhull"

# check Build.PL for dependencies
RDEPEND="!=dev-lang/perl-5.16*
	>=dev-libs/boost-1.55
	|| ( <dev-libs/boost-1.77[threads]
	>dev-libs/boost-1.77 )
	dev-perl/Class-XSAccessor
	dev-perl/Devel-CheckLib
	dev-perl/Devel-Size
	>=dev-perl/Encode-Locale-1.50.0
	dev-perl/IO-stringy
	dev-cpp/tbb
	>=dev-perl/Math-PlanePath-53.0.0
	miniz? ( dev-libs/miniz )
	sci-libs/libigl
	media-libs/nanosvg
	qhull? ( media-libs/qhull[static-libs] )
	>=dev-perl/Moo-1.3.1
	dev-perl/XML-SAX-ExpatXS
	virtual/perl-Carp
	virtual/perl-Encode
	virtual/perl-File-Spec
	virtual/perl-Getopt-Long
	virtual/perl-parent
	virtual/perl-Scalar-List-Utils
	virtual/perl-Test-Simple
	virtual/perl-Thread-Semaphore
	>=virtual/perl-threads-1.960.0
	virtual/perl-Time-HiRes
	virtual/perl-Unicode-Normalize
	virtual/perl-XSLoader
	dev-libs/cereal
	gui? ( dev-perl/Class-Accessor
		dev-perl/Growl-GNTP
		dev-perl/libwww-perl
		dev-perl/Module-Pluggable
		dev-perl/Net-Bonjour
		dev-perl/Net-DBus
		dev-perl/OpenGL
		>=dev-perl/Wx-0.991.800
		dev-perl/Wx-GLCanvas
		>=media-libs/freeglut-3
		virtual/perl-Math-Complex
		>=virtual/perl-Socket-2.16.0
		x11-libs/libXmu
	)"
DEPEND="${RDEPEND}
	sci-libs/nlopt
	dev-cpp/gtest
	dev-perl/Devel-CheckLib
	>=dev-perl/ExtUtils-CppGuess-0.70.0
	>=dev-perl/ExtUtils-Typemaps-Default-1.50.0
	>=dev-perl/ExtUtils-XSpp-0.170.0
	>=dev-perl/Module-Build-0.380.0
	>=dev-perl/Module-Build-WithXSpp-0.140.0
	>=virtual/perl-ExtUtils-MakeMaker-6.800.0
	>=virtual/perl-ExtUtils-ParseXS-3.350.0
	test? (	virtual/perl-Test-Harness
		virtual/perl-Test-Simple )
	media-gfx/openvdb
	x11-libs/wxGTK:${WX_GTK_VER}
	>=sci-mathematics/cgal-5.0.0"

#S="${WORKDIR}/slic3r-prusa3d-9999"

pkg_pretend() {
	einfo "Checking for -std=c++11 support in compiler"
	test-flags-CXX -std=c++11 > /dev/null || die
}

src_unpack() {
	git-r3_src_unpack
}

src_prepare() {
	sed -i lib/Slic3r.pm -e "s@FindBin::Bin@FindBin::RealBin@g" || die
	eapply "${FILESDIR}/${P}-cexpat.patch"
	eapply "${FILESDIR}/${P}-CGAL-5.5.patch"
	eapply "${FILESDIR}/${P}-opencascade.patch"
	if ! use qhull; then
		eapply "${FILESDIR}/${P}-qhull.patch"
	fi
	#if ! use miniz; then
	#	eapply "${FILESDIR}/${P}-miniz.patch"
	#fi
	#eapply "${FILESDIR}/${P}-gtk.patch"
	#eapply "${FILESDIR}/${P}-tbb.patch"
	#eapply "${FILESDIR}/${P}-cgal.patch"
	eapply "${FILESDIR}/${P}-vdb.patch"
	eapply "${FILESDIR}/${P}-setlanguage.patch"
	#eapply "${FILESDIR}/${P}-expat.patch"
	eapply_user
	# drop std=c++11 to compiler defaults...
	sed \
		-e '/c++11/d' \
		-i Build.PL || die
	sed -e '/SLIC3R_FHS_RESOURCES/s[\${CMAKE_INSTALL_FULL_DATAROOTDIR}[/usr/share[' -e '/SLIC3R_FHS_RESOURCES/s[\(DESTINATION "*\)[\1'${D}'[' -i CMakeLists.txt
	#sed -e '/boost::filesystem::path path_resources/s[= .*$[= "/usr/share/slic3r-prusa3d/";[' -i src/slic3r.cpp
}

src_configure() {
	setup-wxwidgets
	default_src_configure
}

src_compile() {
	mkdir build
	cd build
	export CFLAGS="${CFLAGS} -DTBB_HAS_GLOBAL_CONTROL"
	export CXXFLAGS="${CXXFLAGS} -DTBB_HAS_GLOBAL_CONTROL"
	#cmake .. -DCMAKE_INSTALL_PREFIX="${D}/usr" -DCMAKE_BUILD_TYPE=Release -DSLIC3R_FHS=1 -DSLIC3R_WX_STABLE=1 -DSLIC3R_GTK=3 || die
	cmake .. -DCMAKE_INSTALL_PREFIX="${D}/usr" -DCMAKE_BUILD_TYPE=Release -DSLIC3R_FHS=1 -DSLIC3R_GTK=3 || die
	make ${MAKEOPTS} || die
	cd ..
}

src_install() {
	cd build
#	sed -i -e 's@ENV[{]DESTDIR[}]/usr@{CMAKE_INSTALL_PREFIX}@g' -e 's@/usr/lib@\${CMAKE_INSTALL_PREFIX}/lib@' xs/cmake_install.cmake
	make install
	cd ..

	#insinto "${VENDOR_LIB}"
	#doins -r lib/Slic3r.pm lib/Slic3r

	#doins -r /usr/share/resources

	#exeinto "${VENDOR_LIB}"/Slic3r
	#doexe slic3r.pl

	#dosym "${VENDOR_LIB}"/Slic3r/slic3r.pl /usr/bin/slic3r.pl

	#rm -rf "${D}/usr/lib"

	make_desktop_entry prusa-slicer \
		Prusa-Slic3r \
		"${VENDOR_LIB}/Slic3r/resources/icons/Slic3r_128px.png" \
		"Graphics;3DGraphics;Engineering;Development"

}

