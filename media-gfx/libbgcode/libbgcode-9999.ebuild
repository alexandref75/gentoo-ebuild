# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 cmake

DESCRIPTION="Prusa Block & Binary G-code reader / writer / converter"
HOMEPAGE="https://github.com/prusa3d/libbgcode"
EGIT_REPO_URI="https://github.com/prusa3d/libbgcode.git"

LICENSE="AGPL-3 Boost-1.0 GPL-2 LGPL-3 MIT"
SLOT="0"
IUSE="+gui test miniz qhull"

RESTRICT="strip test"

RDEPEND="
	>=media-gfx/heatshrink-0.4.1
	=dev-cpp/catch-2.13.10
"
DEPEND="${RDEPEND}
"

