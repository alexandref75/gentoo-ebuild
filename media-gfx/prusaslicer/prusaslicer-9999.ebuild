# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

WX_GTK_VER="3.2-gtk3"
MY_PN="PrusaSlicer"
MY_PV="$(ver_rs 3 -)"

inherit git-r3 cmake wxwidgets xdg

DESCRIPTION="A mesh slicer to generate G-code for fused-filament-fabrication (3D printers)"
HOMEPAGE="https://www.prusa3d.com/prusaslicer/"
EGIT_REPO_URI="https://github.com/prusa3d/Slic3r.git"

LICENSE="AGPL-3 Boost-1.0 GPL-2 LGPL-3 MIT"
SLOT="0"
IUSE="+gui test miniz qhull"

RESTRICT="test"

RDEPEND="
	dev-cpp/eigen:3
	dev-cpp/tbb:=
	>=dev-libs/boost-1.55:=[nls]
	|| (    <dev-libs/boost-1.77[threads]
			>dev-libs/boost-1.77 )
	dev-libs/cereal
	dev-libs/expat
	dev-libs/glib:2
	dev-libs/gmp:=
	dev-libs/mpfr:=
	>=media-gfx/openvdb-8.2:=
	net-misc/curl[adns]
	media-libs/glew:0=
	media-libs/libjpeg-turbo:=
	media-libs/libpng:0=
	sci-libs/libigl
	sci-libs/nlopt
	>=sci-libs/opencascade-7.6.2:=
	>=sci-mathematics/cgal-5.0:=
	sys-apps/dbus
	sys-libs/zlib:=
	virtual/opengl
	x11-libs/gtk+:3
	x11-libs/wxGTK:${WX_GTK_VER}[X,opengl]
	media-libs/nanosvg
	media-gfx/libbgcode
"
DEPEND="${RDEPEND}
	qhull? ( media-libs/qhull[static-libs] )
"

PATCHES=(
	"${FILESDIR}/${P}-boost-fstream.patch"
	"${FILESDIR}/${P}-cexpat.patch"
	"${FILESDIR}/${P}-CGAL-5.5.patch"
	"${FILESDIR}/${P}-setlanguage.patch"
	"${FILESDIR}/${P}-vdb.patch"
	"${FILESDIR}/${P}-opencascade-7.8.0.patch"
)

# check Build.PL for dependencies
#RDEPEND="
#	!=dev-lang/perl-5.16*
#	dev-perl/Class-XSAccessor
#	dev-perl/Devel-CheckLib
#	dev-perl/Devel-Size
#	>=dev-perl/Encode-Locale-1.50.0
#	dev-perl/IO-stringy
#	>=dev-perl/Math-PlanePath-53.0.0
#	miniz? ( dev-libs/miniz )
#	sci-libs/libigl
#	>=dev-perl/Moo-1.3.1
#	dev-perl/XML-SAX-ExpatXS
#	virtual/perl-Carp
#	virtual/perl-Encode
#	virtual/perl-File-Spec
#	virtual/perl-Getopt-Long
#	virtual/perl-parent
#	virtual/perl-Scalar-List-Utils
#	virtual/perl-Test-Simple
#	virtual/perl-Thread-Semaphore
#	>=virtual/perl-threads-1.960.0
#	virtual/perl-Time-HiRes
#	virtual/perl-Unicode-Normalize
#	virtual/perl-XSLoader
#	gui? ( dev-perl/Class-Accessor
#		dev-perl/Growl-GNTP
#		dev-perl/libwww-perl
#		dev-perl/Module-Pluggable
#		dev-perl/Net-Bonjour
#		dev-perl/Net-DBus
#		dev-perl/OpenGL
#		>=dev-perl/Wx-0.991.800
#		dev-perl/Wx-GLCanvas
#		>=media-libs/freeglut-3
#		virtual/perl-Math-Complex
#		>=virtual/perl-Socket-2.16.0
#		x11-libs/libXmu
#	)"

#DEPEND="${RDEPEND}
#	dev-cpp/gtest
#	dev-perl/Devel-CheckLib
#	>=dev-perl/ExtUtils-CppGuess-0.70.0
#	>=dev-perl/ExtUtils-Typemaps-Default-1.50.0
#	>=dev-perl/ExtUtils-XSpp-0.170.0
#	>=dev-perl/Module-Build-0.380.0
#	>=dev-perl/Module-Build-WithXSpp-0.140.0
#	>=virtual/perl-ExtUtils-MakeMaker-6.800.0
#	>=virtual/perl-ExtUtils-ParseXS-3.350.0
#	test? (	virtual/perl-Test-Harness
#		virtual/perl-Test-Simple )
#	"

#S="${WORKDIR}/slic3r-prusa3d-9999"

pkg_pretend() {
	einfo "Checking for -std=c++11 support in compiler"
	test-flags-CXX -std=c++11 > /dev/null || die
}

src_unpack() {
	git-r3_src_unpack
}

src_prepare() {
	#sed -i lib/Slic3r.pm -e "s@FindBin::Bin@FindBin::RealBin@g" || die
	if ! use qhull; then
		eapply "${FILESDIR}/${P}-qhull.patch"
	fi
	eapply_user
	sed -i -e 's/PrusaSlicer-${SLIC3R_VERSION}+UNKNOWN/PrusaSlicer-${SLIC3R_VERSION}+Gentoo/g' version.inc || die

	sed -i -e 's/find_package(OpenCASCADE 7.6.2 REQUIRED)/find_package(OpenCASCADE REQUIRED)/g' \
		src/occt_wrapper/CMakeLists.txt || die
	cmake_src_prepare
}

src_configure() {
	CMAKE_BUILD_TYPE="Release"

	setup-wxwidgets

	local mycmakeargs=(
		-DOPENVDB_FIND_MODULE_PATH="/usr/$(get_libdir)/cmake/OpenVDB"

		-DSLIC3R_BUILD_TESTS=$(usex test)
		-DSLIC3R_FHS=ON
		-DSLIC3R_GTK=3
		-DSLIC3R_GUI=ON
		-DSLIC3R_PCH=OFF
		-DSLIC3R_STATIC=OFF
		-DSLIC3R_WX_STABLE=ON
		-DSLIC3R_GTK=3
		-Wno-dev
	)

	cmake_src_configure
}
