# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit multilib-minimal cmake

DESCRIPTION="A data compression/decompression library for embedded/real-time systems"
HOMEPAGE="https://github.com/atomicobject/heatshrink"
SRC_URI="https://github.com/atomicobject/heatshrink/archive/refs/tags/v${PV}.tar.gz"

KEYWORDS="~amd64 ~x86"

LICENSE="AGPL-3 Boost-1.0 GPL-2 LGPL-3 MIT"
SLOT="0"

RDEPEND="
"
DEPEND="${RDEPEND}
"

src_prepare() {
	default

	cp "${FILESDIR}/CMakeLists.txt" "${FILESDIR}/Config.cmake.in" .

	cmake_src_prepare
	multilib_copy_sources
}
