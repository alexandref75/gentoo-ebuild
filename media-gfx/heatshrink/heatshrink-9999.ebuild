# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 cmake

DESCRIPTION="A data compression/decompression library for embedded/real-time systems"
HOMEPAGE="https://github.com/atomicobject/heatshrink"
EGIT_REPO_URI="https://github.com/atomicobject/heatshrink.git"

LICENSE="AGPL-3 Boost-1.0 GPL-2 LGPL-3 MIT"
SLOT="0"

RDEPEND="
"
DEPEND="${RDEPEND}
"

