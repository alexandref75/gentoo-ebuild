# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

VCG_VERSION="2023.12"
ISPC_VERSION="v1.18.1"
EMBREE_VERSION="v4.3.1"
LAVMAR_VERSION="2.6.1"
LIBIGL_VERSION="v2.4.0"
STRUCTURESYNTH_VERSION="1.5.1"
TINYGLTF_VERSION="v2.6.3"
U3D_VERSION="1.5.1"
LIBE57FORMAT_VERSION="v2.3.0"
inherit cmake xdg

DESCRIPTION="System for processing and editing unstructured 3D triangular meshes"
HOMEPAGE="https://www.meshlab.net/"
SRC_URI="https://github.com/cnr-isti-vclab/meshlab/archive/refs/tags/MeshLab-${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/cnr-isti-vclab/vcglib/archive/${VCG_VERSION}.tar.gz -> vcglib-${VCG_VERSION}.tar.gz
	https://github.com/ispc/ispc/releases/download/${ISPC_VERSION}/ispc-${ISPC_VERSION}-linux.tar.gz
	https://github.com/embree/embree/archive/refs/tags/${EMBREE_VERSION}.zip ->  embree-${EMBREE_VERSION}.zip
	https://github.com/alemuntoni/levmar/archive/refs/tags/${LAVMAR_VERSION}.zip -> levmar-${LAVMAR_VERSION}.zip
	https://github.com/libigl/libigl/archive/refs/tags/${LIBIGL_VERSION}.zip -> libigl-${LIBIGL_VERSION}.zip
	https://github.com/cnr-isti-vclab/nexus/archive/refs/heads/master.zip -> nexus-master.zip
	https://github.com/cnr-isti-vclab/corto/archive/refs/heads/master.zip -> corto-master.zip
	https://github.com/alemuntoni/StructureSynth/archive/refs/tags/${STRUCTURESYNTH_VERSION}.zip -> StructureSynth-${STRUCTURESYNTH_VERSION}.zip
	https://github.com/syoyo/tinygltf/archive/refs/tags/${TINYGLTF_VERSION}.zip -> tinygltf-${TINYGLTF_VERSION}.zip
	https://github.com/alemuntoni/u3d/archive/refs/tags/${U3D_VERSION}.zip -> u3d-${U3D_VERSION}.zip
	https://github.com/asmaloney/libE57Format/archive/refs/tags/${LIBE57FORMAT_VERSION}.zip -> libE57Format-${LIBE57FORMAT_VERSION}.zip"
S="${WORKDIR}/meshlab-MeshLab-${PV}/src"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="double-precision minimal"

# qhull: newer meshlab supports system re-entrant version
# levmar: build system hardcodes vendored copy
DEPEND="
	dev-cpp/eigen:3
	dev-cpp/muParser
	dev-libs/gmp:=
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtdeclarative:5
	dev-qt/qtnetwork:5
	dev-qt/qtopengl:5
	dev-qt/qtwidgets:5
	dev-qt/qtxml:5
	media-libs/glew:0=
	=media-libs/lib3ds-1*
	media-libs/openctm:=
"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${P}-disable-updates.patch"
	"${FILESDIR}/${P}-c++11-fix.patch"
)
#	"${FILESDIR}/${P}-find-plugins.patch"

src_unpack() {
	unpack ${P}.tar.gz
	cd "${S}" || die
	unpack vcglib-${VCG_VERSION}.tar.gz
	mv vcglib-${VCG_VERSION}/* vcglib || die
	cd "${S}/external" || die
	mkdir downloads
	cd downloads
	unpack ispc-${ISPC_VERSION}-linux.tar.gz
	unpack embree-${EMBREE_VERSION}.zip
	unpack levmar-${LAVMAR_VERSION}.zip
	unpack libigl-${LIBIGL_VERSION}.zip
	unpack nexus-master.zip
	cd nexus-master/src
	unpack corto-master.zip
	cd ../..
	unpack StructureSynth-${STRUCTURESYNTH_VERSION}.zip
	unpack tinygltf-${TINYGLTF_VERSION}.zip
	unpack u3d-${U3D_VERSION}.zip
	unpack libE57Format-${LIBE57FORMAT_VERSION}.zip
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_MINI=$(usex minimal)
		-DBUILD_WITH_DOUBLE_SCALAR=$(usex double-precision)
		-DMESHLAB_USE_DEFAULT_BUILD_AND_INSTALL_DIRS=true
	)
	cmake_src_configure
}
